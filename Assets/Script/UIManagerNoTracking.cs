﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManagerNoTracking : MonoBehaviour
{
    public GameObject target;
    public Text status;
    public Text engagementValue;
    public GameObject applePrefab;
    public Transform spawnPosition;
    public Transform targettedPosition;

    private float currentDistance; 

    private void Start() {
        Weatavix.instance.OnInteractionStateChange += this.interactionStateChanged;
        //Weatavix.instance.contactManager.stopThread();
    }

    private void Update() {
        if (Input.GetKeyDown("space")) {
            this.fullyEngage();
        }

        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.LeftArrow)) {
            this.distanceUp();
        }

        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.RightArrow)) {
            this.distanceDown();
        }

        this.currentDistance = Vector3.Distance(this.target.transform.position, this.targettedPosition.position);
        float parameter = Mathf.InverseLerp(Weatavix.instance.maxContactDistance, 0, this.currentDistance);
        int percentage = ((int)(parameter * 100));
        this.engagementValue.text = percentage + " %";
    }

    public void distanceUp() {
        this.target.transform.position = new Vector3(this.target.transform.position.x, this.target.transform.position.y + 0.005f, this.target.transform.position.z);
    }

    public void distanceDown() {
        this.target.transform.position = new Vector3(this.target.transform.position.x, this.target.transform.position.y - 0.005f, this.target.transform.position.z);
    }

    public void fullyEngage() {
        this.target.transform.position = this.targettedPosition.position;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.GetComponent<CatchableObject>() != null) {
            Destroy(other.gameObject);
            this.spawnNewApple();
        }
    }

    private void spawnNewApple() {
        this.target = Instantiate(this.applePrefab);
        this.target.transform.position = this.spawnPosition.position;
        this.target.transform.SetParent(this.spawnPosition);
        this.target.GetComponent<Rigidbody>().useGravity = false;
    }

    void interactionStateChanged(InteractionState state) {
        switch (state) {
            case InteractionState.SEARCH_TARGETS:
            this.status.text = "SEARCHING";
            break;
            case InteractionState.RELEASE_GRASP:
            this.status.text = "RELEASED";
            break;
            case InteractionState.GRASPED:
            this.status.text = "GRASPED";
            break;
        }
    }
}
