﻿using System.Collections.Generic;
using UnityEngine;
using Pololu.UsbWrapper;
using Pololu.Usc;
using System;

/*
 *      Author : Guillaume Gicquel
 *      Date : 01/02/21
 *      Project : Weatavix (Thomas Howard, Xavier de Tinguy)
 *      Description : Class handling communication with Pololu board and threshold values for the servo
 */

public class ServoManager : MonoBehaviour {

    public ushort maxEngage;
    public ushort maxDisengage;
    public ushort qMinPololu;
    public ushort qMaxPololu;
    public ushort deltaQMax;
    private ushort deltaQ = 0;

    private Usc pololu;


    void Start() {
        this.pololu = this.connectToPololuBoard();
        this.boolEngage(false);
        if (this.maxEngage - this.deltaQMax < this.qMinPololu) {
            Debug.Log("Warning : the negative threshold won't be reached.");
        }
        if (this.maxDisengage > this.qMaxPololu) {
            Debug.Log("Warning : the positive threshold won't be reached.");
        }
    }

    private void OnApplicationQuit() {
        this.disconnectFromPololuBoard();
    }

    public void setVariation(float rate) {
        if (rate >= 1f) {
            rate = 1f;
        } else if (rate <= -1) {
            rate = -1f;
        }

        this.deltaQ = (ushort)((float)this.deltaQMax * rate);
    }

    public void boolEngage(bool value) {
        if (value) this.trySetTarget(0x00, Convert.ToUInt16(this.maxEngage * 4));
        else this.trySetTarget(0x00, Convert.ToUInt16(this.maxDisengage * 4));
    }

    public void updateServo(float rate) {
        this.trySetTarget(0x00, Convert.ToUInt16(((this.maxDisengage - this.maxEngage) * rate + this.maxEngage + this.deltaQ) * 4));
    }

    // Write the current target 1/4µs value to the target channel
    public void trySetTarget(byte channel, ushort target) {
        try {
            this.pololu.setTarget(channel, target);
        } catch {
            //Debug.Log("Failed writing to pololu.");
        }
    }

    private Usc connectToPololuBoard() {
        // Get a list of all connected devices of this type.
        List<DeviceListItem> connectedDevices = Usc.getConnectedDevices();

        foreach (DeviceListItem dli in connectedDevices) {
            Usc device = new Usc(dli);    // Connect to the device.
            return device;                // Return the device.
        }

        Debug.Log("Could not find device. Make sure it is plugged in to USB " +
            "and check your Device Manager (Windows) or run lsusb (Linux).");

        return null;
    }

    private void disconnectFromPololuBoard() {
        try {
            this.pololu.Dispose();
        } catch {
            Debug.Log("Could not disconnect from device. Device may be unreachable.");
        }
    }
}
