﻿using System;
using System.Globalization;
using System.IO;
using UnityEngine;

/**
    29/01/2021 | Authors : 
    - Guillaume Gicquel, CNRS - guillaume.gicquel@irisa.fr
    - Thomas Howard, CNRS - thomas.howard@irisa.fr

    This implementation is used to save and read position values in a .txt file.
**/

public class TextFileParser : MonoBehaviour
{
    private string path;                                   // Path to the txt file containing the position and rotation values
    private string separator;                              // separator between values in same line
    private Quaternion handRotation;                       // Stored value of hand rotation angle with tracker
    private Vector3 positionOffset;                        // Offset tracker / hand
    private Vector3 handScale;
    public bool hasValues = false;

    private void Awake()
    {
        this.path = "Assets/Resources/calibration_vive.txt";
        this.separator = ";";

        this.getStoredValues();
    }

    // Load calibration values from .txt file
    public void getStoredValues()
    {
        StreamReader reader = new StreamReader(this.path);
        NumberFormatInfo nfi = new NumberFormatInfo();
        nfi.NegativeSign = "−";

        try
        {
            string[] values = reader.ReadLine().Split(this.separator.ToCharArray()[0]);
            this.positionOffset = new Vector3(float.Parse(values[0]), float.Parse(values[1]), float.Parse(values[2]));
            this.handRotation = new Quaternion(float.Parse(values[3]), float.Parse(values[4]), float.Parse(values[5]), float.Parse(values[6]));
            this.handScale = new Vector3(float.Parse(values[7]), float.Parse(values[8]), float.Parse(values[9]));
            Debug.Log("Position read : " + this.positionOffset.ToString());
            this.hasValues = true;
        }
        catch (Exception e)
        {
            Debug.LogError("Can't read values in file : " + e.Message);
            this.positionOffset = new Vector3(0, 0, 0);
            this.handScale = new Vector3(1, 1, 1);
        }

        reader.Close();
    }

    // Save calibration's values in the .txt file
    public void saveValues() {
        StreamWriter writer = new StreamWriter(this.path, false);

        writer.WriteLine(this.positionOffset.x + this.separator
                       + this.positionOffset.y + this.separator
                       + this.positionOffset.z + this.separator
                       + this.handRotation.x + this.separator
                       + this.handRotation.y + this.separator
                       + this.handRotation.z + this.separator
                       + this.handRotation.w + this.separator
                       + this.handScale.x + this.separator
                       + this.handScale.y + this.separator
                       + this.handScale.z);

        writer.Close();
        Debug.Log("Saved position and rotation values.");
    }

    public Quaternion getHandRotation() {
        return this.handRotation;
    }

    public Vector3 getHandOffset() {
        return this.positionOffset;
    }

    public Vector3 getHandScale() {
        return this.handScale;
    }

    public void setValues(Vector3 offset, Quaternion rotation, Vector3 scale) {
        this.positionOffset = offset;
        this.handRotation = rotation;
        this.handScale = scale;
    }
}
