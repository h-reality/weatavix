﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.Extras;

/*
 *      Author : Guillaume Gicquel
 *      Date : 01/02/21
 *      Project : Weatavix (Thomas Howard, Xavier de Tinguy)
 *      Description : Calibration script for the Weatavix mounted device
 */

public class Calibration : MonoBehaviour {

    //Class handling reading / writing in the .txt file
    public TextFileParser parser;
    //Virtual markers for sampling positions
    public GameObject virtualCalibL, virtualCalibR, virtualCalibTop, virtualCalibPalm;
    //Transform used to adjust scale
    public GameObject scaleOffset;
    //Transform used to adjust position and rotation
    public Transform handOffset;
    //Text displayed in calibration panel UI
    public Text text;
    //UI buttons for calibration instructions
    public Button btnOK, btnStop;
    //Grasp trigger
    public SteamVR_Action_Boolean graspTrigger;
    //Menu button trigger
    public SteamVR_Action_Boolean menuTrigger;
    //SteamVR controller (left hand)
    public SteamVR_Behaviour_Pose controller;
    public Camera camVR;
    //Parent object of all created sampled points
    public GameObject calibSamples;
    //Material given to sample points markers
    public Material sphereMaterial;
    //Transform containing both canvases
    public GameObject canvases;
    //Image used to display instructions
    public GameObject imgWindow;
    public Image img;
    //3D model of the trackker
    public GameObject trackerModel;
    //calibration marker sphere on the bottom of the controller
    public GameObject calibSphere;

    private float virtualHandWidth, virtualHandHeight, handSampledWidth, handSampledHeight;
    private GameObject calibL, calibR, calibTop, calibPalm;
    private bool isCalibDone = false;
    private bool isCalibMenuOn = false;
    //events fired by buttons
    private UnityEvent okEvent, stopEvent;
    //position sampled when grasp press
    private Vector3 samplePosition = Vector3.zero;


    private void Awake() {
        this.okEvent = new UnityEvent();
        this.stopEvent = new UnityEvent();

        this.canvases.SetActive(false);
        this.imgWindow.SetActive(false);

        //Hide virtual calibration points
        this.virtualCalibL.SetActive(false);
        this.virtualCalibR.SetActive(false);
        this.virtualCalibPalm.SetActive(false);
        this.virtualCalibTop.SetActive(false);
        //this.tutoPic.gameObject.SetActive(false);

        //Calculate width and height of the virtual hand
        this.virtualHandWidth = Vector3.Distance(this.virtualCalibL.transform.position, this.virtualCalibR.transform.position);
        this.virtualHandHeight = Vector3.Distance(this.virtualCalibTop.transform.position, this.virtualCalibPalm.transform.position);

        this.trackerModel.SetActive(false);
        this.calibSphere.SetActive(false);
    }

    private void Start() {
        this.stopCalibrationProcess();

        //If the parser already has values stored in a txt file, calibrate the device with those values.
        //Otherwise, start the calibration process
        if (this.parser.hasValues) {
            this.calibrate(this.parser.getHandOffset(), this.parser.getHandRotation(), this.parser.getHandScale());
        } else {
            this.startCalibrationProcess();
        }
    }

    private void Update() {
        if (this.graspTrigger.GetStateUp(SteamVR_Input_Sources.Any)) {
            if (this.isCalibMenuOn) {
                this.sampleCurrentPosition();
            }
        }

        if (this.menuTrigger.GetStateUp(SteamVR_Input_Sources.Any)) {
            if (!this.isCalibMenuOn) {
                this.startCalibrationProcess();
            }
        }
    }

    //Get the position of the calibration sphere, at the bottom of the controller
    private void sampleCurrentPosition() {
        this.samplePosition = this.controller.GetComponent<ControllerSampling>().sampleSphere.transform.position;
    }

    private void startCalibrationProcess() { 
        //Set the panel text
        if (this.isCalibDone || this.isCalibMenuOn) {
            this.text.text = "Do you want to start calibration ?";
        } else {
            this.text.text = "Let's calibrate the Weatavix !";
        }

        //Register laser pointer events
        this.controller.GetComponent<SteamVR_LaserPointer>().PointerIn += this.PointerInside;
        this.controller.GetComponent<SteamVR_LaserPointer>().PointerOut += this.PointerOutside;
        this.controller.GetComponent<SteamVR_LaserPointer>().PointerClick += this.PointerClick;

        this.trackerModel.SetActive(true);
        this.calibSphere.SetActive(true);

        this.isCalibDone = false;
        this.isCalibMenuOn = true;
        Weatavix.instance.animator.SetBool("isCalibrating", this.isCalibMenuOn);

        this.okEvent.AddListener(this.calibratePalmPoint);
        this.stopEvent.AddListener(this.stopCalibrationProcess);

        this.canvases.SetActive(true);
        this.canvases.transform.rotation = Quaternion.LookRotation(this.camVR.transform.forward, Vector3.up);
        Vector3 canvasHeight = this.canvases.transform.GetChild(0).position;
        this.canvases.transform.GetChild(0).position = new Vector3(canvasHeight.x, this.camVR.transform.position.y, canvasHeight.z);
        this.btnStop.gameObject.SetActive(true);
        this.btnOK.gameObject.SetActive(true);
    }

    private void stopCalibrationProcess() {
        this.okEvent.RemoveAllListeners();
        this.stopEvent.RemoveAllListeners();

        this.canvases.SetActive(false);
        this.imgWindow.SetActive(false);
        this.btnStop.gameObject.SetActive(false);
        this.btnOK.gameObject.SetActive(false);
        this.isCalibMenuOn = false;
        Weatavix.instance.animator.SetBool("isCalibrating", this.isCalibMenuOn);
        this.trackerModel.SetActive(false);
        this.calibSphere.SetActive(false);

        //destroy all sample points
        foreach (Transform child in this.calibSamples.transform) {
            GameObject.Destroy(child.gameObject);
        }
    }

    private void calibratePalmPoint() {
        this.imgWindow.SetActive(true);
        this.btnOK.gameObject.SetActive(false);
        this.text.text = "Points are sampled with the bottom part of the controller.\n\n" +
            "Start by putting your controller between your major thumb pad and your outer pad.\n\n" +
            "Once in position, press the grasp button on your controller.";
        this.StartCoroutine(this.waitForSample(this.calibrateLeftPoint, this.virtualCalibPalm));
        this.popImage("Images/calibPalm");
    }

    private void calibrateLeftPoint() {
        this.text.text = "Now place the controller on the side of your index finger, alongside the first phalange.\n\n" +
            "Once in position, press the grasp button on your controller.";
        this.StartCoroutine(this.waitForSample(this.calibrateRightPoint, this.virtualCalibL));
        this.popImage("Images/calibIndex");
    }

    private void calibrateRightPoint() {
        this.text.text = "Now place the controller on the side of your little finger, alongside the second phalange.\n\n" +
            "Once in position, press the grasp button on your controller.";
        this.StartCoroutine(this.waitForSample(this.calibrateTopPoint, this.virtualCalibR));
        this.popImage("Images/calibLittleFinger");
    }

    private void calibrateTopPoint() {
        this.text.text = "Now place the controller on the tip of your major.\n\n" +
            "Once in position, press the grasp button on your controller.";
        this.StartCoroutine(this.waitForSample(this.computeSampleValues, this.virtualCalibTop));
        this.popImage("Images/calibMajorTip");
    }

    private void popImage(string imgPath) {
        this.img.sprite = Resources.Load<Sprite>(imgPath);
    }

    private void computeSampleValues() {
        //POSITION

        //Get the middle point between L & R calibration points, deduct position offset
        Vector3 centerPos = (this.calibL.transform.position + this.calibR.transform.position) / 2;
        Vector3 sampledOffset = centerPos - this.handOffset.position;

        //ANGLE

        //Get the normal vector (up direction) of the hand
        Vector3 sampledHandNormal = Vector3.Cross((this.calibL.transform.position - this.calibPalm.transform.position),
                                                  (this.calibR.transform.position - this.calibPalm.transform.position)).normalized;

        Quaternion handRotation = Quaternion.LookRotation((this.calibL.transform.position - this.calibR.transform.position).normalized, sampledHandNormal);
        this.handOffset.transform.rotation = handRotation;

        Quaternion localRotation = this.handOffset.transform.localRotation;


        //SCALE
        this.handSampledWidth = Vector3.Distance(this.calibL.transform.position, this.calibR.transform.position);
        this.handSampledHeight = Vector3.Distance(this.calibTop.transform.position, this.calibPalm.transform.position);

        float widthFactor = this.handSampledWidth / this.virtualHandWidth;
        float heightFactor = this.handSampledHeight / this.virtualHandHeight;

        Vector3 scaleOffset = new Vector3(heightFactor, (heightFactor + widthFactor) / 2, widthFactor);

        this.saveValues(sampledOffset, localRotation, scaleOffset);
        this.calibrate(sampledOffset, localRotation, scaleOffset);
    }

    private void saveValues(Vector3 handOffset, Quaternion handRotation, Vector3 scale) {
        this.parser.setValues(handOffset, handRotation, scale);
        this.parser.saveValues();
    }

    private void calibrate(Vector3 handOffset, Quaternion handRotation, Vector3 scale) {
        this.isCalibDone = true;
        this.handOffset.transform.localPosition = Vector3.zero;
        this.handOffset.localPosition += handOffset;
        this.handOffset.transform.localRotation = handRotation;
        //this.scaleOffset.transform.localScale = scale;
        this.stopCalibrationProcess();
    }



    //Displays current requested calibration point, wait for the user to takke the sample then calls the callback function
    private IEnumerator waitForSample(UnityAction callback, GameObject calibPoint) {
        //Pop image
        //this.tutoPic.gameObject.SetActive(true);

        this.samplePosition = Vector3.zero;
        calibPoint.SetActive(true);

        //Wait for sample
        while (this.samplePosition == Vector3.zero) {
            yield return null;
        }

        //Create a visual sphere on the sampled point
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.position = this.samplePosition;
        sphere.transform.SetParent(this.calibSamples.transform);
        sphere.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        sphere.GetComponent<Renderer>().material = this.sphereMaterial;

        //Match the sphere created to the desired object
        if (calibPoint == this.virtualCalibPalm) {
            this.calibPalm = sphere;
        } else if (calibPoint == this.virtualCalibL) {
            this.calibL = sphere;
        } else if (calibPoint == this.virtualCalibR) {
            this.calibR = sphere;
        } else if (calibPoint == this.virtualCalibTop) {
            this.calibTop = sphere;
        }

        //Hide virtual calib point
        calibPoint.SetActive(false);
        callback.Invoke();
    }





    // LASER POINTER EVENTS

    public void PointerClick(object sender, PointerEventArgs e) {
        if (e.target.gameObject.GetComponent<Button>() != null) {
            if (e.target.gameObject.GetComponent<Button>().interactable) {
                e.target.gameObject.GetComponent<Button>().onClick.Invoke();
            }
        }
    }

    public void PointerInside(object sender, PointerEventArgs e) {
        if (e.target.gameObject.GetComponent<Button>() != null) {
            EventSystem.current.SetSelectedGameObject(e.target.gameObject);
            e.target.gameObject.GetComponent<Button>().OnSelect(new BaseEventData(EventSystem.current));
        }
    }

    public void PointerOutside(object sender, PointerEventArgs e) {
        if (e.target.gameObject.GetComponent<Button>() != null) {
            EventSystem.current.SetSelectedGameObject(null);
            e.target.gameObject.GetComponent<Button>().OnDeselect(new BaseEventData(EventSystem.current));
        }
    }

    public void okClickEvent() {
        this.okEvent.Invoke();
    }

    public void stopClickEvent() {
        this.stopEvent.Invoke();
    }
}
