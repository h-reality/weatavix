﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ControllerSampling : MonoBehaviour {

    public SteamVR_Behaviour_Pose controller;
    public Calibration calibration;
    public GameObject sampleSphere;

    private void Awake() {
        this.calibration.controller = this.controller;
    }
}
