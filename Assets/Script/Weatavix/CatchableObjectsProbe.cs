﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchableObjectsProbe : MonoBehaviour {
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<CatchableObject>() != null) {
            Weatavix.instance.AddTarget(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject.GetComponent<CatchableObject>() != null) {
            Weatavix.instance.RemoveTarget(other.gameObject);
        }
    }
	
}
