﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *      Author : Guillaume Gicquel
 *      Date : 01/02/21
 *      Project : Weatavix (Thomas Howard, Xavier de Tinguy)
 *      Description : Manager script for Weatavix (Singleton)
 */

[RequireComponent(typeof(ServoManager))]
[RequireComponent(typeof(ContactManager))]
[RequireComponent(typeof(CatchableObjectsProbe))]
public class Weatavix : MonoBehaviour {
    [HideInInspector]
    public static Weatavix instance;
    public event OnInteractionStateChangeDelegate OnInteractionStateChange;
    public delegate void OnInteractionStateChangeDelegate(InteractionState newState);

    public Animator animator;
    public GameObject targetPosition;
    [Range(0.05f, 0.5f)]
    public float maxContactDistance;
    [Range(0f, 0.1f)]
    public float maxDistanceThreshold;
    [Range(-0.1f, 0.1f)]
    public float zBackHandThreshold;
    [Range(0f, 0.1f)]
    public float lateralScale;
    [Range(0f, 0.1f)]
    public float releaseDelay;
    [Range(0f, 0.5f)]
    public float smoothTime;
    [Range(0f, 0.01f)]
    public float maxDisplacement;
    public bool isGrabbed = false, isReleased = true, isPalmTouching = false;

    private float commandServo;
    private float tempCommandServo;
    private float timerTransition;
    private float transitionTime;
    private bool boolTransitionEnabled;

    private List<GameObject> potentialTargetSpheres = new List<GameObject>();
    private Vector3 relativePositionWhenGrasped;
    private Vector3 estimatedPosition;

    private ServoManager servoManager;
    public ContactManager contactManager;
    private GameObject targettedObject;
    private InteractionState currentInteractionState;
    private InteractionState lastInteractionState;
    private List<Vector3> handLastPositions = new List<Vector3>();
    private Vector3 lastHandPosition;
    private Vector3 handMeanSpeed;
    private float normalScale = 1.0f;
    private float timerRelease;



    private void Awake() {
        //Singleton declaration
        if (Weatavix.instance != null) {
            GameObject.Destroy(this);
        } else {
            instance = this;
            GameObject.DontDestroyOnLoad(this);
        }

        this.servoManager = this.GetComponent<ServoManager>();
        this.contactManager = this.GetComponent<ContactManager>();
    }

    void Start() {
        this.currentInteractionState = InteractionState.SEARCH_TARGETS;
        this.lastInteractionState = this.currentInteractionState;
    }

    void LateUpdate() {
        this.updateHandSpeed();
        this.animator.SetBool("isGrabbed", this.isGrabbed);

        switch (this.currentInteractionState) {
            case InteractionState.SEARCH_TARGETS:
            this.currentInteractionState = this.searchTarget();
            break;

            case InteractionState.GRASPED:
            this.currentInteractionState = this.grasped();
            break;

            case InteractionState.RELEASE_GRASP:
            this.currentInteractionState = this.releaseGrasp();
            break;

        }

        //If interaction state has changed
        if (this.currentInteractionState != this.lastInteractionState) {
            //memorize current interaction state
            this.lastInteractionState = this.currentInteractionState;
            //if something registered to the state changing event, fires it
            OnInteractionStateChange?.Invoke(this.currentInteractionState);
        }

        //update servo command
        if (this.boolTransitionEnabled) {
            this.timerTransition += Time.deltaTime;
            float alpha = this.timerTransition / this.transitionTime;
            if (alpha >= 1f) {
                this.updateServo(this.commandServo);
                this.boolTransitionEnabled = false;
            } else {
                this.updateServo((1 - alpha / this.transitionTime) * this.tempCommandServo + alpha * this.commandServo);
            }
        } else {
            this.updateServo(this.commandServo);
        }
    }

    //Calculate mean speed of the hand based on the 5 last frames positions
    private void updateHandSpeed() {
        int nbFrames = 5;

        this.handLastPositions.Add((this.transform.position - this.lastHandPosition) / Time.deltaTime);
        if (this.handLastPositions.Count > nbFrames) this.handLastPositions.RemoveAt(0);
        this.handMeanSpeed = Vector3.zero;

        foreach (Vector3 posiion in this.handLastPositions) {
            this.handMeanSpeed += posiion;
        }

        this.handMeanSpeed /= this.handLastPositions.Count;
        this.lastHandPosition = this.transform.position;
    }

    private void updateServo(float distanceV) {
        if (distanceV > this.maxDistanceThreshold) {
            this.servoManager.updateServo(1);
        } else if (distanceV >= 0) {
            this.servoManager.updateServo(distanceV / this.maxDistanceThreshold);
        }
    }

    private void SmoothTransitionServo(float tempCommand, float transitionTime) {
        this.tempCommandServo = tempCommand;
        this.transitionTime = transitionTime;
        this.timerTransition = 0f;
        this.boolTransitionEnabled = true;
    }

    //Actuator is grasped by the user between fingers & palm
    private InteractionState grasped() {
        this.commandServo = 0;

        //If the user releases grasp during the grasped state
        if (!this.isGrabbed) {
            this.timerRelease = 0;
            this.estimatedPosition = Vector3.zero;
            return InteractionState.RELEASE_GRASP;
        }

        return InteractionState.GRASPED;
    }

    //User has released the grasp on the actuator
    private InteractionState releaseGrasp() {
        float scoreDistanceEstimated;

        //If the user grasps it again
        if (this.isGrabbed) {
            this.StartCoroutine(this.SlowDriftToPosition());
            return InteractionState.GRASPED;
        }

        this.estimatedPosition += this.targetPosition.transform.InverseTransformVector(this.handMeanSpeed) * Time.deltaTime;
        this.timerRelease += Time.deltaTime;

        //Estimate the command to send to the servo, but disengage if the target is behind the user's hand
        if (this.estimatedPosition.z >= 0) {
            scoreDistanceEstimated = Mathf.Sqrt(this.lateralScale * (Mathf.Pow(this.estimatedPosition.x, 2) + Mathf.Pow(this.estimatedPosition.y, 2)) + this.normalScale * Mathf.Pow(this.estimatedPosition.z, 2));
        } else if (this.estimatedPosition.z > -this.zBackHandThreshold) {
            scoreDistanceEstimated = Mathf.Sqrt(this.lateralScale * (Mathf.Pow(this.estimatedPosition.x, 2) + Mathf.Pow(this.estimatedPosition.y, 2)));
        } else {
            scoreDistanceEstimated = this.maxDistanceThreshold;
        }

        this.commandServo = scoreDistanceEstimated;

        //If the user has released the object in the timeframe, consider it released and switch interaction state to idle
        if (this.timerRelease > this.releaseDelay) {
            this.StopCoroutine(this.SlowDriftToPosition());
            this.SmoothTransitionServo(this.commandServo, this.smoothTime);
            this.servoManager.setVariation(0);
            this.releaseObject(this.handMeanSpeed);
            this.isReleased = true;
            return InteractionState.SEARCH_TARGETS;
        }

        return InteractionState.RELEASE_GRASP;
    }

    private InteractionState searchTarget() {

        List<GameObject> discardList = new List<GameObject>();
        GameObject tempTarget = null;
        float distClosest = 10;
        float distTemp;

        foreach (GameObject potentialTarget in this.potentialTargetSpheres) {
            if (potentialTarget != null) {
                //Get distance between hand & object
                distTemp = Vector3.Distance(potentialTarget.transform.position, this.targetPosition.transform.position);

                //Get the closest object of the list
                if (distTemp < distClosest) {
                    distClosest = distTemp;
                    tempTarget = potentialTarget;
                }
            } else {
                //If target is null, then it's already out of reach, prepare discard
                discardList.Add(potentialTarget);
            }
        }

        //Clear the target list of unreachable objects
        foreach (GameObject lostObject in discardList) {
            this.potentialTargetSpheres.Remove(lostObject);
        }

        //If an object has been defined as the closest
        if (tempTarget != null) {
            float scoreDistance;

            this.targettedObject = tempTarget;
            Vector3 localFramePosition = this.targetPosition.transform.InverseTransformPoint(tempTarget.transform.position);

            //Define the approximated position to send to the servo
            if (localFramePosition.z >= 0) {
                //close
                scoreDistance = Mathf.Sqrt(this.lateralScale * (Mathf.Pow(localFramePosition.x, 2) + Mathf.Pow(localFramePosition.y, 2)) + this.normalScale * Mathf.Pow(localFramePosition.z, 2));
            } else if (localFramePosition.z > this.zBackHandThreshold) {
                //very close
                scoreDistance = Mathf.Sqrt(this.lateralScale * (Mathf.Pow(localFramePosition.x, 2) + Mathf.Pow(localFramePosition.y, 2)));
            } else {
                //far away
                scoreDistance = this.maxDistanceThreshold;
            }

            this.commandServo = scoreDistance;

            if ((distClosest < this.maxContactDistance) && this.isGrabbed) {
                //close enough
                this.initiateContact();
                this.isReleased = false;
                return InteractionState.GRASPED;
            }
        } else {
            this.commandServo = this.maxDistanceThreshold;
        }
        return InteractionState.SEARCH_TARGETS;
    }

    private void initiateContact() {
        this.relativePositionWhenGrasped = this.targetPosition.transform.InverseTransformPoint(this.targettedObject.transform.position);
        //Stick the catchable object to the virtual hand with parenting
        this.targettedObject.GetComponent<Rigidbody>().isKinematic = true;
        this.targettedObject.transform.parent = this.targetPosition.transform;
        this.StartCoroutine(this.SlowDriftToPosition());
    }

    //Release virtual link between the catchable object and the hand, reapply physics to object
    private void releaseObject(Vector3 handVelocity) {
        this.targettedObject.transform.SetParent(null);
        this.targettedObject.GetComponent<Rigidbody>().isKinematic = false;
        this.targettedObject.GetComponent<Rigidbody>().useGravity = true;
        this.targettedObject.GetComponent<Rigidbody>().velocity = handVelocity;
        this.targettedObject = null;
    }

    public void AddTarget(GameObject og) {
        this.potentialTargetSpheres.Add(og);
    }

    public void RemoveTarget(GameObject og) {
        this.potentialTargetSpheres.Remove(og);
    }






    public IEnumerator SlowDriftToPosition() {
        float t = 0;
        while (t <= 0.2f) {
            t += Time.deltaTime / 0.2f;
            this.targettedObject.transform.localPosition = Vector3.Lerp(this.relativePositionWhenGrasped, Vector3.zero, t);
            yield return null;
        }
        this.targettedObject.transform.localPosition = Vector3.zero;
        yield return null;
    }
}








public enum InteractionState {
    SEARCH_TARGETS, RELEASE_GRASP, GRASPED
}
