﻿using System;
using System.Collections;
using System.IO.Ports;
using System.Threading;
using UnityEngine;

/*
 *      Author : Guillaume Gicquel
 *      Date : 01/02/21
 *      Project : Weatavix (Thomas Howard, Xavier de Tinguy)
 *      Description : Class handling proximity contacts with the Weatavix's actuated sphere and the serial com with the Arduino.
 */

public class ContactManager : MonoBehaviour {
    public string arduinoCOM;
    public int contactThreshold;
    public bool looping = true;

    public int fingersCapacityValue;
    public int palmCapacitiveValue;

    private SerialPort stream;
    private Thread thread;

    public bool IsLooping() {
        lock (this) {
            return this.looping;
        }
    }


    private void Start() {
        //Run communication thread
        this.thread = new Thread(this.threadLoop);
        this.thread.Start();
    }

    private void LateUpdate() {
        if (this.fingersCapacityValue > this.contactThreshold) {
            Weatavix.instance.isGrabbed = true;
        } else {
            Weatavix.instance.isGrabbed = false;
        }

        if (this.palmCapacitiveValue > this.contactThreshold) {
            Weatavix.instance.isPalmTouching = true;
        } else {
            Weatavix.instance.isPalmTouching = false;
        }
    }

    public void threadLoop() {
        this.connectToArduino();

        // Looping
        while (this.IsLooping()) {
            //Don't know why, but we have to write a character to the arduino serial port to be able to read it afterwise...
            this.writeToArduino("a");

            // Read from Arduino
            string result = this.readFromArduino();

            if (result != null) {
                var sArray = result.Split('/');
                this.fingersCapacityValue = int.Parse(sArray[0]);
                this.palmCapacitiveValue = int.Parse(sArray[1]);
                this.stream.DiscardInBuffer();
            }
        }

        Debug.Log("stream closing");
        this.stream.Close();
    }

    public void connectToArduino() {
        if (int.Parse(this.arduinoCOM) >= 0) {
            this.stream = new SerialPort("COM" + this.arduinoCOM, 115200);
            this.stream.ReadTimeout = 50;

            try {
                this.stream.Open();
                Debug.Log("Communication port with Arduino is ready.");
            } catch (Exception e) {
                Debug.LogError(e.Message);
                throw;
            }
        } else {
            Debug.Log("Unable to open communication port with Arduino.");
        }
    }

    public string readFromArduino(int timeout = 50) {
        try {
            return this.stream.ReadLine();
        } catch (TimeoutException e) {
            return null;
        } catch (Exception e) {
            Debug.Log(e.Message);
            return null;
        }
    }

    public void writeToArduino(string message) {
        if (this.stream != null) {
            this.stream.WriteLine(message);
            this.stream.BaseStream.Flush();
        }
    }

    public void stopThread() {
        lock (this) {
            this.looping = false;
        }
    }

    private void OnApplicationQuit() {
        this.stopThread();
    }
}
