﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereSpawner : MonoBehaviour
{
    public GameObject catchableSpherePrefab;
    public Vector3 spawnPosition;



    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.GetComponent<CatchableObject>() != null) {
            Destroy(collision.gameObject);
            Instantiate(this.catchableSpherePrefab, this.spawnPosition, Quaternion.identity, null);
        }
    }
}
